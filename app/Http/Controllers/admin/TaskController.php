<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\TaskDetail;
// use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return Inertia::render('Tasks/Create', compact('users'));
        // return auth()->user()->id;
        // $tasks =Task::all();
        // return $tasks[0]->fromUser->name;
        // return auth()->user()->tasks;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'title' => ['required'],
            'dirct' => ['required'],
            // 'details' => ['required'],
            'to_user_id' => ['required'],
        ]);
        $data["from_user_id"] = auth()->user()->id;

        Request::file('file') !== null ? $file = Request::file('file')->store('topics', 'public') : $file = null ;
        $data["file"] = $file;

        Task::create($data);

        return Redirect::route('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::findOrFail($id)->load('fromUser', 'toUser');
        $taskDateCreate = $task['created_at']->addMinutes(180)->toDateTimeString();
        // return $task;

        $taskDetails = TaskDetail::where('task_id', $id)->get()->load('fromUser')->map (function($task) {
            return [
                "id" => $task->id,
                'details' => $task->details,
                'file' => $task->file,
                'created_at' => $task->created_at->addMinutes(180)->toDateTimeString(),
                'from_user' => $task->fromUser,
            ];
        });
        // return $task;
        // return $taskDetails;
        $file = asset('/storage/' . $task->file);
        return Inertia::render('Tasks/Show', compact('task', 'file', 'taskDetails', 'taskDateCreate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($id)
    {
        $task = Task::findOrFail($id);
        // return null;
        if ($task->file !== null) {
            return response()->download(public_path('/storage/' . $task->file));
        } else {
            return 'no file';
        }
        return $task;
        // return $task->file;

    }
}
