<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\TaskDetail;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


class TaskDetailsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $data = request()->validate([
            'details' => ['required'],
        ]);
        $data["task_id"] = $id;
        $data["from_user_id"] = auth()->user()->id;

        Request::file('file') !== null ? $file = Request::file('file')->store('topics', 'public') : $file = null ;
        $data["file"] = $file;

        // return $data;
        TaskDetail::create($data);

        return Redirect::route('task-show', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($id)
    {
        $TaskDetail = TaskDetail::findOrFail($id);
        // return null;
        if ($TaskDetail->file !== null) {
            return response()->download(public_path('/storage/' . $TaskDetail->file));
        } else {
            return 'no file';
        }
        return $TaskDetail;
        // return $task->file;

    }
}
