<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Inertia\Inertia;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function main()
    {
        switch  (auth()->user()->role) {
            case 100:
                $tasks = Task::all()->load('fromUser', 'toUser')->map (function($task) {
                    return [
                        "id" => $task->id,
                        'title' => $task->title,
                        'dirct' => $task->dirct,
                        'details' => $task->details,
                        'status' => $task->status,
                        'file' => $task->file,
                        'created_at' => $task->created_at->addMinutes(180)->toDateTimeString(),
                        'from_user' => $task->fromUser,
                        'to_user' => $task->toUser
                    ];
                });
                // return $tasks;
                break;
            //
            //
            //
            case 99:
                $tasks = Task::where('to_user_id', auth()->user()->id)->get()->load('fromUser', 'toUser')->map (function($task) {
                    return [
                        "id" => $task->id,
                        'title' => $task->title,
                        'dirct' => $task->dirct,
                        'details' => $task->details,
                        'status' => $task->status,
                        'file' => $task->file,
                        'created_at' => $task->created_at->addMinutes(180)->toDateTimeString(),
                        'from_user' => $task->fromUser,
                        'to_user' => $task->toUser
                    ];
                });
                // return $tasks;
                break;
            //
            //
            //
            case 1:
                $tasks = Task::where('to_user_id', auth()->user()->id)->get()->load('fromUser', 'toUser')->map (function($task) {
                    return [
                        "id" => $task->id,
                        'title' => $task->title,
                        'dirct' => $task->dirct,
                        'details' => $task->details,
                        'status' => $task->status,
                        'file' => $task->file,
                        'created_at' => $task->created_at->addMinutes(180)->toDateTimeString(),
                        'from_user' => $task->fromUser,
                        'to_user' => $task->toUser
                    ];
                });
                // return $tasks;
                break;
            //
            //
            //
            case 0:
                $tasks = Task::where('to_user_id', auth()->user()->id)->get()->map (function($task) {
                    return [
                        "id" => $task->id,
                        'title' => $task->title,
                        'dirct' => $task->dirct,
                        'details' => $task->details,
                        'status' => $task->status,
                        'file' => $task->file,
                        'created_at' => $task->created_at->addMinutes(180)->toDateTimeString(),
                        'from_user' => $task->fromUser,
                        'to_user' => $task->toUser
                    ];
                });
                // return $tasks;
                break;
    }
    return Inertia::render('Dashboard', compact('tasks'));


    }
}
