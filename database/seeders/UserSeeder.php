<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            // 'email_verified_at' => Carbon::now(),
            'password' => bcrypt('e4a'), // password
            'role' => '100',
        ]);
        DB::table('users')->insert([
            'name' => 'م.أسامة',
            'email' => 'osama@gmail.com',
            // 'email_verified_at' => Carbon::now(),
            'password' => bcrypt('e4a'), // password
            'role' => '99',
        ]);
        DB::table('users')->insert([
            'name' => 'م.سامي',
            'email' => 'sami@gmail.com',
            // 'email_verified_at' => Carbon::now(),
            'password' => bcrypt('e4a'), // password
            'role' => '99',
        ]);
        DB::table('users')->insert([
            'name' => 'محمد عمار عمار',
            'email' => 'm.ammar4web@gmail.com',
            // 'email_verified_at' => Carbon::now(),
            'password' => bcrypt('e4a'), // password
            'role' => '1',
        ]);
        DB::table('users')->insert([
            'name' => 'غسان',
            'email' => 'user.com',
            // 'email_verified_at' => Carbon::now(),
            'password' => bcrypt('e4a'), // password
            'role' => '0',
        ]);
    }
}
