<?php

use App\Http\Controllers\admin\TaskController;
use App\Http\Controllers\admin\TaskDetailsController;
use App\Http\Controllers\DashboardController;
use App\Models\Task;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Auth/Login', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', [DashboardController::class, 'main'])->middleware('auth')->name('dashboard');

Route::get('/task-create', [TaskController::class, 'create'])->name('task-create')->middleware(['auth', 'verified']);
Route::post('/task-store', [TaskController::class, 'store'])->name('task-store');
Route::get('/task-show/{id}', [TaskController::class, 'show'])->name('task-show');
Route::get('/task-download/{id}', [TaskController::class, 'download'])->name('task-download');
Route::post('/task-create-details/{id}', [TaskDetailsController::class, 'store'])->name('task-create-details');
Route::get('/task-download-details/{id}', [TaskDetailsController::class, 'download'])->name('task-download-details');

require __DIR__.'/auth.php';
