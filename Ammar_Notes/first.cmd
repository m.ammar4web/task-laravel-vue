@REM on the local environment
composer create-project laravel/laravel Laravel-vue-tasks

git init --initial-branch=main
git remote add origin https://gitlab.com/m.ammar4web/task-laravel-vue.git
git add README.md
git commit -m "add README"
git push -u origin main

@REM on the VPS server environment
git init
git switch -c main
git remote add origin https://gitlab.com/m.ammar4web/task-laravel-vue.git
git pull origin main

@REM on the local environment
composer require laravel/breeze --dev
php artisan breeze:install vue

git add *
git commit -m "create a new Laravel application with breeze"
git push

@REM on the VPS server environment
@REM create .env
git pull origin main
composer install
npm install
npm run build

@REM on the local environment
@REM edit 'AppServiceProvider.php' file to fix a Default Error When Migrating To MySQL Database
@REM edit .env
php artisan migrate
@REM git add, commit, push

@REM on the VPS server environment
git pull origin main
php artisan migrate
